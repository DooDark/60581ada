-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Мар 01 2021 г., 05:46
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `Кофейня`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Заказ`
--

CREATE TABLE `Заказ` (
  `id` int NOT NULL,
  `UserID` int NOT NULL,
  `Started_at` time NOT NULL,
  `Finished_at` time NOT NULL,
  `Status` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `ЗаказМеню`
--

CREATE TABLE `ЗаказМеню` (
  `id` int NOT NULL,
  `ID_Order` int NOT NULL,
  `ID_Menu` int NOT NULL,
  `quantity` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `Классификация`
--

CREATE TABLE `Классификация` (
  `id` int NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Классификация`
--

INSERT INTO `Классификация` (`id`, `Name`, `Description`) VALUES
(1, 'Кофе', 'Всевозможные сорта кофе. Эксклюзивное кофе. Кофе в турке. Все это вы можете заказать прямо сейчас.'),
(2, 'Какао и шоколад', 'Вкусные и согревающие в холодную погоду напитки.'),
(3, 'Завтраки', 'Вкусный и бодрящий завтрак.'),
(4, 'Десерты', 'Десерты которых вы еще не пробовали. Прекрасные и вкусные, легкие и нежные.');

-- --------------------------------------------------------

--
-- Структура таблицы `Меню`
--

CREATE TABLE `Меню` (
  `id` int NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Price` int NOT NULL,
  `Description` varchar(255) NOT NULL,
  `Volume` int NOT NULL,
  `Class_ID` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `Меню`
--

INSERT INTO `Меню` (`id`, `Name`, `Price`, `Description`, `Volume`, `Class_ID`) VALUES
(1, 'Латте', 220, 'Легкий кофе с молоком.', 200, 1),
(2, 'Капучино', 150, 'Кофе с молоком', 200, 1),
(3, 'Экспрессо', 110, 'Крепкий итальянский кофе', 50, 1),
(4, 'Американо', 130, 'Экспрессо разбавленный кипятком.)', 150, 1),
(5, 'Темный шоколад', 120, 'Крепкий согревающий шоколад', 50, 2),
(6, 'Какао с маршмеллоу ', 230, 'Какао напиток с добавлением маршмеллоу\r\n', 300, 2),
(7, 'Сырники с английским соусом', 320, 'Сырники с сладким нежным соусом.', 280, 3),
(8, 'Закусочный круассан', 90, 'Булочка с сладким соусом', 70, 3),
(9, 'Трюфельный торт', 260, 'Мусс на основе горького бельгийского шоколада.', 120, 4),
(10, 'Йогуртово-фруктовый торт', 250, 'Легкий йогуртовый мусс с тонкой ноткой миндаля.', 130, 4);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Заказ`
--
ALTER TABLE `Заказ`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `ЗаказМеню`
--
ALTER TABLE `ЗаказМеню`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ID_Menu` (`ID_Menu`),
  ADD KEY `ID_Order` (`ID_Order`);

--
-- Индексы таблицы `Классификация`
--
ALTER TABLE `Классификация`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `Меню`
--
ALTER TABLE `Меню`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ibfk_1` (`Class_ID`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Классификация`
--
ALTER TABLE `Классификация`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `Меню`
--
ALTER TABLE `Меню`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `ЗаказМеню`
--
ALTER TABLE `ЗаказМеню`
  ADD CONSTRAINT `ЗаказМеню_ibfk_1` FOREIGN KEY (`ID_Menu`) REFERENCES `Меню` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `ЗаказМеню_ibfk_2` FOREIGN KEY (`ID_Order`) REFERENCES `Заказ` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `Меню`
--
ALTER TABLE `Меню`
  ADD CONSTRAINT `ibfk_1` FOREIGN KEY (`Class_ID`) REFERENCES `Классификация` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
