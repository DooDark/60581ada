<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class cofee extends Seeder
{
	public function run()
	{
			$data = [

							 'Name_CL' => 'Кофе',
							 'Description_CL'=>'Всевозможные сорта кофе. Эксклюзивное кофе. Кофе в турке. Все это вы можете заказать прямо сейчас.',
			 ];
			 $this->db->table('classifire')->insert($data);

			 $data = [

 							 'Name_CL' => 'Какао и шоколад',
 							 'Description_CL'=>'Вкусные и согревающие в холодную погоду напитки.',
 			 ];
 			 $this->db->table('classifire')->insert($data);

			 $data = [

 							 'Name_CL' => 'Завтраки',
 							 'Description_CL'=>'Вкусный и бодрящий завтрак.',
 			 ];
 			 $this->db->table('classifire')->insert($data);

			 $data = [

 							 'Name_CL' => 'Десерты',
 							 'Description_CL'=>'Десерты которых вы еще не пробовали. Прекрасные и вкусные, легкие и нежные.',
 			 ];
 			 $this->db->table('classifire')->insert($data);

       $data = [

                'name' => 'Тимофей',
                'Price' => 1,
                'description'=>'д/с Огонек',
                'Volume' => 1,
                "Class_ID" => 1,
        ];
        $this->db->table('menu1')->insert($data);

        $data = [

                'name' => 'Тохтик',
                'Price' => 1,
                'description'=>'д/с Тохтик Огонек',
                'Volume' => 1,
                "Class_ID" => 4,
        ];
        $this->db->table('menu1')->insert($data);
}
}
