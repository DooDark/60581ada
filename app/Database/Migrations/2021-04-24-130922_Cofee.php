<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Cofee extends Migration
{
	public function up()
	{
				if (!$this->db->tableexists('classifire'))
				{
					// Setup Keys
					$this->forge->addkey('cid', TRUE);

					$this->forge->addfield(array(
						'cid' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
						'Name_CL' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
						'Description_CL' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
					));
					$this->forge->createtable('classifire', TRUE);
				}

        // activity_type
        if (!$this->db->tableexists('menu1'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
								'Price' => array('type' => 'INT', 'null' => FALSE),
                'description' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
								'Volume' => array('type' => 'INT', 'null' => FALSE),
                'Class_ID' => array('type' => 'INT', 'null' => TRUE),
            ));
            $this->forge->addForeignKey('Class_ID','classifire','cid','RESRICT','RESRICT');
            // create table
            $this->forge->createtable('menu1', TRUE);
        }


	}

	//--------------------------------------------------------------------

	public function down()
	{
         $this->forge->droptable('menu1');
	}
}
