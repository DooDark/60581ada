<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Addpictureurlfield extends Migration
{
	public function up()
    {
        if ($this->db->tableexists('menu1'))
        {
            $this->forge->addColumn('menu1',array(
                'picture_url' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
        }
    }
    public function down()
    {
        $this->forge->dropColumn('menu1', 'picture_url');
    }
}
