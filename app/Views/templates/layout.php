<!DOCTYPE html>
<head>
    <title>Кофейня у DJA</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/6e9b058a28.js"></script><!--ICON-->
    <meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/style.css" type="text/css">
    <style>
        main{
            margin-top: 90px;
        }
        .bga {
          color: rgb(220,220,220);
          padding-top: 12px;
          padding-bottom: 1px;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-lg">
    <a class="navbar-brand fa fas fa-spider fa-2x" style="color:white" href="<?= base_url()?>"></a>
    <a class="navbar-brand" href="<?= base_url()?>">Dja</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse d-flex justify-content-between" id="navbarNav">
      <ul class="navbar-nav">
      </ul>
      <div class="d-none">Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
      <ul class="navbar-nav">
        <?php if (! $ionAuth->loggedIn()): ?>
            <div class="nav-item dropdown">
              <a class="nav-link active" href="<?= base_url()?>/auth/login"><span class="fas fa fa-sign-in-alt" style="color:white"></span>&nbsp;&nbsp;Вход</a>
            </div>
        <?php else: ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true" ><span class="fas fa fa-user-alt" style="color:white"></span>&nbsp;&nbsp;  <?php echo $ionAuth->user()->row()->email; ?></a>
                <div class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdown01">
                    <?php if ($ionAuth->user()->row()->email == "admin@admin.com"):?>
                        <a class="dropdown-item" href="<?= base_url()?>/menu/viewAllWithCathegory"><span class="fas fa fa-pencil-square-o " style="color:white"></span>&nbsp;&nbsp;Редактировать</a>
                        <a class="dropdown-item" href="<?= base_url()?>/menu/store"><span class="fas fa fa-plus-circle  " style="color:white"></span>&nbsp;&nbsp;Добавить новое</a>
                    <?php endif?>
                    <a class="dropdown-item" href="<?= base_url()?>/auth/logout"><span class="fas fa fa-sign-in-alt" style="color:white"></span>&nbsp;&nbsp;Выход</a>
              </div>
            </li>
        <?php endif ?>
        </ul>
    </div>
  </div>
</nav>
<main id="main" role="main">
    <?php if (session()->getFlashdata('message')) :?>
      <div class="alert alert-info" role="alert" style="max-width: 540px;">
          <?= session()->getFlashdata('message') ?>
      </div>
    <?php endif ?>

    <?= $this->renderSection('content') ?>
</main>
<footer class="text-center bga bg-dark">
    <p>© Александров Даниил 2021&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
</footer>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
</body>
</html>
