<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
  <style>
    .bb {
      max-height: 95vh;
      min-height: 95vh;
      min-width: 100vw;
    }

    .txt {
      margin: 1px;
      width: 100%;
      text-align: center;
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
    }

    .bgr {
      background: rgba(30, 30, 30, 0.8);
    }

    .btn {
      margin-top: 5px;
      border-radius:15px;
      background: rgb(220,220,220);
      color: rgb(30,30,30);
    }

    #div {
      padding: 0;
    }
  </style>
    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item bb a-block active">
            <img src="/first.jpg" class="d-block w-100" alt="...">
            <div id="div" class="carousel-caption d-none top-50 start-50 translate-middle d-md-block bgr">
              <div class="txt">
                <h5>Кофейня у DJA</h5>
                <p>Это иновационное место, использующее новейшие технологии для приготовления пищи.</p>
                <?php if(!$ionAuth->loggedIn()): ?>
                  <a aria-current="page" href="/auth/login"><div class="btn">Сделать заказ</div></a>
                <?php else: ?>
                  <a aria-current="page" href="/menu"><div class="btn">Сделать заказ</div></a>
                <?php endif ?>
              </div>
            </div>
        </div>
        <div class="carousel-item bb">
            <img src="/second.jpg" class="d-block w-100" alt="...">
            <div id="div" class="carousel-caption d-none top-50 start-50 translate-middle d-md-block bgr">
              <div class="txt">
                <h5>Только у нас вы можете</h5>
                <p>Пропробовать кофе обжаренное и сваренное на разогнанных процессорах.</p>
                <?php if(! $ionAuth->loggedIn()): ?>
                  <a aria-current="page" href="/auth/login"><div class="btn">Сделать заказ</div></a>
                <?php else: ?>
                  <a aria-current="page" href="/menu"><div class="btn">Сделать заказ</div></a>
                <?php endif ?>
              </div>
            </div>
        </div>
        <div class="carousel-item bb">
            <img src="/theard.jpg" class="d-block w-100" alt="...">
            <div id="div" class="carousel-caption d-none top-50 start-50 translate-middle d-md-block bgr">
              <div class="txt">
                <h5>Позавтракать</h5>
                <p>Едой сваренной при помощи гидравлического нагрева.</p>
                <?php if(! $ionAuth->loggedIn()): ?>
                  <a aria-current="page" href="/auth/login"><div class="btn">Сделать заказ</div></a>
                <?php else: ?>
                  <a aria-current="page" href="/menu"><div class="btn">Сделать заказ</div></a>
                <?php endif ?>
              </div>
            </div>
        </div>
        <div class="carousel-item bb">
            <img src="/four.jpg" class="d-block w-100" alt="...">
            <div id="div" class="carousel-caption d-none top-50 start-50 translate-middle d-md-block bgr">
              <div class="txt">
                <h5>И так же сможете попробовать</h5>
                <p>Десерты приготовленные LEGO-роботом, чей код написсан на ассемблере.</p>
                <?php if(! $ionAuth->loggedIn()): ?>
                  <a aria-current="page" href="/auth/login"><div class="btn">Сделать заказ</div></a>
                <?php else: ?>
                  <a aria-current="page" href="/menu"><div class="btn">Сделать заказ</div></a>
                <?php endif ?>
              </div>
            </div>
        </div>
      </div>
      <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>
    </div>
<?= $this->endSection() ?>
