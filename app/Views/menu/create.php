<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container mt-5" style="max-width: 540px;">

    <?= form_open_multipart('menu/store'); ?>
    <div class="form-group">
        <label for="name">Имя</label>
        <input type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" name="name"
               value="<?= old('name'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('name') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="name">Описание</label>
        <input type="text" class="form-control <?= ($validation->hasError('description')) ? 'is-invalid' : ''; ?>" name="description"
               value="<?= old('description') ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('description') ?>
        </div>

    </div>
    <div class="form-group">
        <label for="name">Цена</label>
        <input type="number" class="form-control <?= ($validation->hasError('Price')) ? 'is-invalid' : ''; ?>" name="Price"
                 value="<?= old('Price') ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('Price') ?>
        </div>

    </div>
    <div class="form-group">
        <label for="name">Объем</label>
        <input type="number" class="form-control <?= ($validation->hasError('Volume')) ? 'is-invalid' : ''; ?>" name="Volume"
               value="<?= old('Volume') ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('Volume') ?>
        </div>

    </div>
    <div class="form-group">
        <label for="name">Классификация</label>
        <input type="number" class="form-control <?= ($validation->hasError('Class_ID')) ? 'is-invalid' : ''; ?>" name="Class_ID"
               value="<?= old('Class_ID') ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('Class_ID') ?>
        </div>

    </div>

    <div class="form-group">
        <label for="birthday">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
        <div class="invalid-feedback">
            <?= $validation->getError('picture') ?>
        </div>
    </div>

    <div class="form-group mb-5 mt-1">
    <button type="submit" class="btn btn-primary" name="submit">Создать</button>
    </div>
    </form>


    </div>
<?= $this->endSection() ?>
