<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($Menu)) : ?>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">
                        <img height="150" src="..." class="card-img" alt="<?= esc($Menu['Name']); ?>">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($Menu['Name']); ?></h5>
                            <p class="card-text"><?= esc($Menu['Description']); ?></p>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Цена:</div>
                                <div class="text-muted"><?= esc($Menu['Price']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Объем:</div>
                                <div class="text-muted"><?= esc($Menu['Volume']); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <?php else : ?>
        <p>Рейтинг не найден.</p>
    <?php endif ?>
</div>
<?= $this->endSection() ?>
