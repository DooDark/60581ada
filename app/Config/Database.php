<?php

namespace Config;

use CodeIgniter\Database\Config;

/**
 * Database Configuration
 */
class Database extends Config
{
	/**
	 * The directory that holds the Migrations
	 * and Seeds directories.
	 *
	 * @var string
	 */
	public $filesPath = APPPATH . 'Database' . DIRECTORY_SEPARATOR;

	/**
	 * Lets you choose which connection group to
	 * use if no other is specified.
	 *
	 * @var string
	 */
	public $defaultGroup = 'default';

	/**
	 * The default database connection.
	 *
	 * @var array
	 */
	public $default = [
		//'DSN'      => 'postgresql:host=ec2-3-217-219-146.compute-1.amazonaws.com;port=5432;dbname=daqt4ghivqrea3;user=tyutubixvpegee;password=420000d402bb54e446c4a08351b4221310520de112aba9e4ddde478d6d6319fb',
		'DSN' 		 => 'Postgre://tyutubixvpegee:420000d402bb54e446c4a08351b4221310520de112aba9e4ddde478d6d6319fb@ec2-3-217-219-146.compute-1.amazonaws.com:5432/daqt4ghivqrea3',
		//'hostname' => 'localhost',
		'hostname' => 'ec2-3-217-219-146.compute-1.amazonaws.com',
		'username' => 'tyutubixvpegee',
		'password' => '420000d402bb54e446c4a08351b4221310520de112aba9e4ddde478d6d6319fb',
		'database' => 'daqt4ghivqrea3',
		'DBDriver' => 'Postgre',
    //'DBDriver' => 'pdo',
		'DBPrefix' => '',
		'pConnect' => TRUE,
		'DBDebug'  => TRUE,
		'charset'  => 'utf8',
		'DBCollat' => 'utf8_general_ci',
		'swapPre'  => '',
		'encrypt'  => false,
		'compress' => false,
		'strictOn' => false,
		'failover' => [],
		//'port'     => 3306,
    'port'     => 5432,
	];

	/**
	 * This database connection is used when
	 * running PHPUnit database tests.
	 *
	 * @var array
	 */
	public $tests = [
		'DSN'      => '',
		'hostname' => '127.0.0.1',
		'username' => '',
		'password' => '',
		'database' => ':memory:',
		'DBDriver' => 'SQLite3',
		'DBPrefix' => 'db_',  // Needed to ensure we're working correctly with prefixes live. DO NOT REMOVE FOR CI DEVS
		'pConnect' => false,
		'DBDebug'  => (ENVIRONMENT !== 'production'),
		'charset'  => 'utf8',
		'DBCollat' => 'utf8_general_ci',
		'swapPre'  => '',
		'encrypt'  => false,
		'compress' => false,
		'strictOn' => false,
		'failover' => [],
		'port'     => 3306,
	];

	//--------------------------------------------------------------------

	public function __construct()
	{
		parent::__construct();

		// Ensure that we always set the database group to 'tests' if
		// we are currently running an automated test suite, so that
		// we don't overwrite live data on accident.
		if (ENVIRONMENT === 'testing')
		{
			$this->defaultGroup = 'tests';
		}
	}

	//--------------------------------------------------------------------

}
