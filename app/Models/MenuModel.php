<?php namespace App\Models;
use CodeIgniter\Model;
class MenuModel extends Model
{
    protected $table = 'menu1'; //таблица, связанная с моделью
    protected $allowedFields = ['name', 'description', 'Price', 'Volume', 'Class_ID', 'picture_url'];

    public function getElementMenu($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getElementWithCathegory($id = null, $search = '')
    {
        $builder = $this->select('*')->join('classifire','menu1.Class_ID = classifire.cid')->like('name', $search,'both', null, true)->orlike('description',$search,'both',null,true);
        if (!is_null($id))
        {
            return $builder->where(['menu1.id' => $id])->first();
        }
        return $builder;
    }
}
