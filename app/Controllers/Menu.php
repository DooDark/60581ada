<?php namespace App\Controllers;

use App\Models\MenuModel;
use Aws\S3\S3Client;

class Menu extends BaseController


{
 public function index() //Обображение всех записей
 {
   if(!$this->ionAuth->loggedIn()){
     return redirect()->to('auth/login');
   }

   $model = new MenuModel();
   $data ['menu1'] = $model->getElementMenu();
   echo view('menu/view_all', $this->withIon($data));
 }

 public function view($id = null) //отображение одной записи
 {
   if(!$this->ionAuth->loggedIn()){
     return redirect()->to('auth/login');
   }
   $model = new MenuModel();
   $data ['menu1'] = $model->getElementMenu($id);
   echo view('menu/view', $this->withIon($data));
 }

  public function viewAllWithCathegory()
  {
      if ($this->ionAuth->isAdmin())
      {
        $perPage = 0;
        if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
          {
              //сохранение кол-ва страниц в переменной сессии
              session()->setFlashdata('per_page', $this->request->getPost('per_page'));
              $per_page = $this->request->getPost('per_page');
          }
          else {
              $per_page = session()->getFlashdata('per_page');
              session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
              if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
          }
          $data['per_page'] = $per_page;

          if (!is_null($this->request->getPost('search')))
          {
              session()->setFlashdata('search', $this->request->getPost('search'));
              $search = $this->request->getPost('search');
          }
          else {
              $search = session()->getFlashdata('search');
              session()->setFlashdata('search', $search);
              if (is_null($search)) $search = '';
          }
          $data['search'] = $search;

          helper(['form','url']);
          $model = new MenuModel();
          $data['menu1'] = $model->getElementWithCathegory(null, $search)->paginate($per_page, 'group1');
          $data['pager'] = $model->pager;
          echo view('menu/view_all_with_cathegory', $this->withIon($data));
      }
      else
      {
          //session()->setFlashdata('message', lang('menu'));
          return redirect()->to('/auth/login');
      }
  }

  public function create()
  {
      if (!$this->ionAuth->loggedIn())
      {
          return redirect()->to('/auth/login');
      }
      helper(['form']);
      $data ['validation'] = \Config\Services::validation();
      echo view('menu/create', $this->withIon($data));
  }

  public function store()
  {
      helper(['form','url']);

      if ($this->request->getMethod() === 'post' && $this->validate([
              'name' => 'required|min_length[3]|max_length[255]',
              'description'  => 'required',
              'Price'  => 'required',
              'Volume'  => 'required',
              'Class_ID'  => 'required',
              'picture'  => 'is_image[picture]|max_size[picture,1024]',
          ]))
      {
        $insert = null;
        $file = $this->request->getFile('picture');
        if ($file->getSize() != 0) {
            //подключение хранилища
            $s3 = new S3Client([
                'version' => 'latest',
                'region' => 'us-east-1',
                'endpoint' => getenv('S3_ENDPOINT'),
                'use_path_style_endpoint' => true,
                'credentials' => [
                    'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                    'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                ],
            ]);
            //получение расширения имени загруженного файла
            $ext = explode('.', $file->getName());
            $ext = $ext[count($ext) - 1];
            //загрузка файла в хранилище
            $insert = $s3->putObject([
                'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                //генерация случайного имени файла
                'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                'Body' => fopen($file->getRealPath(), 'r+')
            ]);

        }

          $model = new MenuModel();
          $data = [
              'name' => $this->request->getPost('name'),
              'description' => $this->request->getPost('description'),
              'Price' => $this->request->getPost('Price'),
              'Volume' => $this->request->getPost('Volume'),
              'Class_ID' => $this->request->getPost('Class_ID'),
          ];

          if (!is_null($insert))
            $data['picture_url'] = $insert['ObjectURL'];
          $model->save($data);
          //session()->setFlashdata('message', lang('Curating.rating_create_success'));
          return redirect()->to('/menu');
      }
      else
      {
          return redirect()->to('/menu/create')->withInput();
      }
  }

  public function edit($id)
  {
      if (!$this->ionAuth->loggedIn())
      {
          return redirect()->to('/auth/login');
      }
      $model = new MenuModel();

      helper(['form']);
      $data ['menu1'] = $model->getElementMenu($id);
      $data ['validation'] = \Config\Services::validation();
      echo view('menu/edit', $this->withIon($data));

  }

  public function update()
  {
      helper(['form','url']);
      echo '/menu/edit/'.$this->request->getPost('id');
      if ($this->request->getMethod() === 'post' && $this->validate([
              'id'  => 'required',
              'name' => 'required|min_length[3]|max_length[255]',
              'description'  => 'required',
              'Price'  => 'required',
              'Volume'  => 'required',
              'Class_ID'  => 'required',
          ]))
      {
          $model = new MenuModel();
          $model->save([
              'id' => $this->request->getPost('id'),
              'name' => $this->request->getPost('name'),
              'description' => $this->request->getPost('description'),
              'Price' => $this->request->getPost('Price'),
              'Volume' => $this->request->getPost('Volume'),
              'Class_ID' => $this->request->getPost('Class_ID'),
          ]);
          //session()->setFlashdata('message', lang('Curating.rating_update_success'));

          return redirect()->to('/menu');
      }
      else
      {
         return redirect()->to('/menu/edit/'.$this->request->getPost('id'))->withInput();
      }
  }

  public function delete($id)
  {
      if (!$this->ionAuth->loggedIn())
      {
          return redirect()->to('/auth/login');
      }
      $model = new MenuModel();
      $model->delete($id);
      return redirect()->to('/menu');
  }
}
